package com.yextremedev.claimsystem.commands;

import com.yextremedev.claimsystem.main.ClaimSystem;
import org.bukkit.entity.Player;

public abstract class Command {

    public Command() {
        ClaimSystem.getCommandRegister().register(this);
    }

    public abstract String getName();

    public abstract void execute(Player player);

}
