package com.yextremedev.claimsystem.commands.defaults;

import com.yextremedev.claimsystem.chunk.ChunkData;
import com.yextremedev.claimsystem.commands.Command;
import com.yextremedev.claimsystem.main.ClaimSystem;
import com.yextremedev.claimsystem.utils.Messages;
import org.bukkit.entity.Player;

public class ClaimCommand extends Command {


    @Override
    public String getName() {
        return "claim";
    }

    @Override
    public void execute(Player player) {
        ChunkData data = ClaimSystem.getChunkManager().getChunkData(player.getLocation().getChunk());
        if (!data.isClaimed()) {
            data.setOwner(player.getName());
            player.sendMessage(Messages.CHUNK_CLAIMED);
        } else {
            player.sendMessage(String.format(Messages.CHUNK_ALREADY_CLAIMED, data.getOwner()));
        }
    }

}
