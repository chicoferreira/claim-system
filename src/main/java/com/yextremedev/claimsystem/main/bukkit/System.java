package com.yextremedev.claimsystem.main.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

public class System {

    public CraftWorld getWorld(String name) {
        return (CraftWorld) Bukkit.getWorld(name);
    }

    public CraftChunk getChunk(String name, int x, int z) {
        return (CraftChunk) this.getWorld(name).getChunkAt(x, z);
    }

}
