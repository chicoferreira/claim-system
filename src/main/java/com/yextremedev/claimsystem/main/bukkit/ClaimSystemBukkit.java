package com.yextremedev.claimsystem.main.bukkit;

import com.yextremedev.claimsystem.main.ClaimSystem;
import org.bukkit.plugin.java.JavaPlugin;

public class ClaimSystemBukkit extends JavaPlugin {

    private final ClaimSystem claimSystem;

    public ClaimSystemBukkit() {
        this.claimSystem = new ClaimSystem();
    }

    @Override
    public void onEnable() {
        claimSystem.enable();
    }

    @Override
    public void onDisable() {
        claimSystem.disable();
    }
}
