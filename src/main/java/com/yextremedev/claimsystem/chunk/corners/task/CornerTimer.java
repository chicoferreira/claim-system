package com.yextremedev.claimsystem.chunk.corners.task;

import org.bukkit.entity.Player;

import java.util.Timer;

public class CornerTimer {

    private final Timer timer;
    private final Player player;
    private CornerTask cornerTask;

    public CornerTimer(Player player) {
        this.timer = new Timer();
        this.player = player;
    }

    public CornerTimer start() {
        cornerTask = new CornerTask(player);
        timer.schedule(cornerTask, 0, 100);
        return this;
    }

    public void stop() {
        cornerTask.cancel();
        timer.cancel();
    }

    public boolean isRunning() {
        return cornerTask.isRunning();
    }
}
