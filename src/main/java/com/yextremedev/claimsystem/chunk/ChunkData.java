package com.yextremedev.claimsystem.chunk;

import com.yextremedev.claimsystem.chunk.corners.ChunkCorner;
import com.yextremedev.claimsystem.chunk.dto.ChunkDto;
import com.yextremedev.claimsystem.main.ClaimSystem;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import java.util.Set;

public class ChunkData {

    private final String world;
    private final int x;
    private final int z;
    private String owner;
    private ChunkCorner corner;

    public ChunkData(String world, int x, int z) {
        this.world = world;
        this.x = x;
        this.z = z;
    }

    public ChunkData(ChunkDto dto) {
        this.world = dto.getWorld();
        this.owner = dto.getOwner();
        this.x = dto.getX();
        this.z = dto.getZ();
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void removeOwner() {
        this.setOwner(null);
    }

    public boolean isClaimed() {
        return this.getOwner() != null;
    }


    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }


    /**
     * @see CraftChunk#getBlock(int, int, int)
     */
    public Location get(int x, double y, int z) {
        return new Location(this.getWorld(), this.getX() << 4 | x & 15, y, this.getZ() << 4 | z & 15);
    }


    public ChunkCorner getChunkCorner() {
        if (corner == null) {
            corner = new ChunkCorner(this);
        }
        return corner;
    }

    public Set<Location> getCorners(double y) {
        return new ChunkCorner(this).getCorners(y);
    }

    public CraftWorld getWorld() {
        return ClaimSystem.getSystem().getWorld(world);
    }

    public CraftChunk toBukkit() {
        return ClaimSystem.getSystem().getChunk(world, getX(), getZ());
    }

    public boolean equals(String world, int x, int z) {
        return this.world.equals(world) &&
                this.getX() == x &&
                this.getZ() == z;
    }

    public String getWorldName() {
        return this.world;
    }
}
