package com.yextremedev.claimsystem.chunk.dto;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "chunkDatas", noClassnameStored = true)
public class ChunkDto {

    @Id
    private final ObjectId objectId;

    private String world;

    private String owner;

    private int x;

    private int z;

    public ChunkDto() {
        this.objectId = new ObjectId();
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getX() {
        return x;
    }

    public ChunkDto setX(int x) {
        this.x = x;
        return this;
    }

    public int getZ() {
        return z;
    }

    public ChunkDto setZ(int z) {
        this.z = z;
        return this;
    }
}
