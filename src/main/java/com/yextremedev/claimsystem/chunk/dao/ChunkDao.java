package com.yextremedev.claimsystem.chunk.dao;

import com.yextremedev.claimsystem.chunk.dto.ChunkDto;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class ChunkDao extends BasicDAO<ChunkDto, String> {

    public ChunkDao(Datastore ds) {
        super(ChunkDto.class, ds);
    }

}
