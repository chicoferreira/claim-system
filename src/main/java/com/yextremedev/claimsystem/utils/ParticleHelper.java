package com.yextremedev.claimsystem.utils;

import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ParticleHelper {

    public static void sendParticle(Player player, Location location) {
        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(EnumParticle.EXPLOSION_NORMAL, true, location.getBlockX(), location.getBlockY(), location.getBlockZ(), 0, 0, 0, 0, 0);

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

}
