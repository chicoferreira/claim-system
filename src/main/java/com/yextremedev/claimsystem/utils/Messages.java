package com.yextremedev.claimsystem.utils;

public class Messages {

    public static final String CHUNK_ALREADY_CLAIMED = "§cEssa chunk já está claimada pelo %s.";
    public static final String CHUNK_CLAIMED = "§eChunk claimada com sucesso.";
    public static final String NOT_PLAYER = "§cVocê precisa de ser um jogador para executar esse comando.";
    public static final String CHUNK_UNCLAIMED = "§aChunk unclaimada com sucesso.";
    public static final String CHUNK_NOT_OWNER = "§cVocê precisa de ser o dono desta chunk. O dono atual é %s.";
    public static final String CHUNK_NOT_CLAIMED = "§cEssa chunk não está claimada.";
    public static final String CHUNK_CLAIMED_BY = "§eA chunk x: §f%s §ez: §f%s §eestá claimada pelo §f%s§e.";
    public static final String CHUNK_NOT_CLAIMED_BY = "§eA chunk x: §f%s §ez: §f%s §enão está claimada.";
    public static final String CHUNK_CORNERS_START = "§eAgora você está a ver as bordas da sua chunk.";
    public static final String CHUNK_CORNERS_STOP = "§eAgora você não está a ver mais as bordas da sua chunk.";
    public static final String ERROR_OCCURRED_PLAYER = "§cOcorreu um erro ao executar esse comando.";
    public static final String ERROR_OCCURRED_CONSOLE = "Ocorreu um erro enquanto %s tentava executar um comando:";
}
